package com.iconvn.signalsr

import com.ale.infra.contact.IRainbowContact

class AppUtl {


    companion object {
        private var instance: AppUtl? = null

        fun getInstance() = instance ?: synchronized(this) {
            instance ?: AppUtl().also { instance = it }
        }
    }

    fun isEmailFormat(emailAddress: String): Boolean {
        val validEmailPattern = Regex("[a-zA-z0-9._-]+@[a-z]+\\.+[a-z]+")
        if (emailAddress.isEmpty()) {
            return false
        }
        return emailAddress.trim().matches(validEmailPattern)
    }

}

