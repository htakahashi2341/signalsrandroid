package com.iconvn.signalsr

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.location.Geocoder
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.widget.TextView
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.ale.listener.SignoutResponseListener
import com.ale.rainbowsdk.RainbowSdk
import com.google.android.gms.location.*
import java.util.*

class MainActivity : AppCompatActivity() {
    private val TAG: String = ">>> MainActivity"
    private var mLocationCallback: LocationCallback? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (BuildConfig.DEBUG) {Log.d(TAG, "onCreate")}

        if ((resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) == Configuration.UI_MODE_NIGHT_YES) {
            setTheme(R.style.DarkTheme)
        } else {
            setTheme(R.style.LightTheme)
        }
        if (BuildConfig.DEBUG) {
            Log.d(
                TAG,
                "UI_MODE_NIGHT_MASK= ${resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK}"
            )
        }
        setContentView(R.layout.activity_main)
        val navController = findNavController(R.id.containerFragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navHome, R.id.navSettings
            )
        )


        setupActionBarWithNavController(navController, appBarConfiguration)
        val navView: BottomNavigationView = findViewById(R.id.navView)
        navView.setupWithNavController(navController)

        this.supportActionBar?.hide()
        if (BuildConfig.DEBUG) {Log.d(TAG, "state= ${lifecycle.currentState}")}

/*
        val name = "signalsr"
        val descriptionText = "SIGNALS R Notification"
        val importance = NotificationManager.IMPORTANCE_HIGH
        val channel = NotificationChannel(AppConstants.CHANNEL_ID, name, importance).apply {
            description = descriptionText
        }

        val notificationManager: NotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
*/
        // Passing listener to start function

        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (BuildConfig.DEBUG) {Log.d(TAG, "LocationCallBack()")}

                locationResult ?: return
                AppData.getInstance().setLatitude(locationResult.lastLocation.latitude)
                AppData.getInstance().setLongitude(locationResult.lastLocation.longitude)


                if (RainbowSdk.instance().connection().isConnected) {
                    try {
                        val geoCoder = Geocoder(this@MainActivity)
                        val addressList = geoCoder.getFromLocation(AppData.getInstance().getLatitude()!!, AppData.getInstance().getLongitude()!!, 1)
                        var addressLine1: String = ""
                        val subThoroughfare: String? = addressList[0].subThoroughfare
                        subThoroughfare?.let { addressLine1 = it }
                        val thoroughfare: String? = addressList[0].thoroughfare
                        thoroughfare?.let { addressLine1 = "$addressLine1 $it" }
                        val locality: String? = addressList[0].locality
                        locality?.let { addressLine1 = "$addressLine1 $it" }

                        var addressLine2: String = ""
                        val subAdminArea: String? = addressList[0].subAdminArea
                        subAdminArea?.let { addressLine2 = it }
                        val adminArea: String? = addressList[0].adminArea
                        adminArea?.let { addressLine2 = "$addressLine2 $it" }
                        val postalCode: String? = addressList[0].postalCode
                        postalCode?.let { addressLine2 = "$addressLine2 $it" }
                        val countryCode: String? = addressList[0].countryCode
                        countryCode?.let { addressLine2 = "$addressLine2 $it" }

                        val textLocationAddress: TextView? = findViewById<TextView>(R.id.textLocationAddress1)
                        textLocationAddress?.let {
                            it.text = addressLine1
                        }
                        val textLocationAddress2: TextView? = findViewById<TextView>(R.id.textLocationAddress2)
                        textLocationAddress2?.let {
                            it.text = addressLine2
                        }
                    } catch(e: Exception) {
                        if (BuildConfig.DEBUG) {Log.d(TAG, "LocationCallBack() Geocoder Exception")}
                    }

                }
            }
        }

        if (!isPermissionsGranted()) {
            requestPermissions()
            return
        }
        requestLocationUpdate()

        val sharedPreferences: SharedPreferences = getSharedPreferences(AppConstants.KEY_APP_DATA, Context.MODE_PRIVATE)
        AppData.getInstance().setSigninEmail(sharedPreferences.getString(AppConstants.KEY_SIGNIN_EMAIL, null))
        if (BuildConfig.DEBUG) Log.d(TAG, "onCreate SignInEmail= ${sharedPreferences.getString(AppConstants.KEY_SIGNIN_EMAIL, "No Email")} ")
        AppData.getInstance().setSigninPassword(sharedPreferences.getString(AppConstants.KEY_SIGNIN_PASSWORD, null))
        AppData.getInstance().setSignalsEmail(sharedPreferences.getString(AppConstants.KEY_SIGNALS_EMAIL, null))


    }

    override fun onResume() {
        super.onResume()
        if (BuildConfig.DEBUG) {Log.d(TAG, "onResume")}

    }

    override fun onPause() {
        super.onPause()
        if (BuildConfig.DEBUG) {Log.d(TAG, "onPause")}

    }

    public override fun onStart() {
        super.onStart()
        if (BuildConfig.DEBUG) {Log.d(TAG, "onStart")}

    }


    public override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop")
        if (RainbowSdk.instance().connection().isConnected) {
            val editor = getSharedPreferences(AppConstants.KEY_APP_DATA, Context.MODE_PRIVATE).edit()
            editor.putString(AppConstants.KEY_SIGNIN_EMAIL, AppData.getInstance().getSigninEmail())
            editor.putString(AppConstants.KEY_SIGNIN_PASSWORD, AppData.getInstance().getSigninPassword())
            editor.putString(AppConstants.KEY_SIGNALS_EMAIL, AppData.getInstance().getSignalsEmail())
            editor.putString(AppConstants.KEY_SIGNALS_JID, AppData.getInstance().getSignalsContact()?.jid)
            editor.apply()
            if (BuildConfig.DEBUG) Log.d(TAG, "onStop Signin Email= ${AppData.getInstance().getSigninEmail()} ")
        }

    }

    public override fun onDestroy() {
        super.onDestroy()
        if (BuildConfig.DEBUG) Log.d(TAG, "onDestroy Signin Status = ${RainbowSdk.instance().connection().isConnected}")
        if (RainbowSdk.instance().connection().isConnected) {
            RainbowSdk.instance().connection().signout(object : SignoutResponseListener() {

                override fun onSignoutSucceeded() {
                    if (BuildConfig.DEBUG) Log.d(TAG, "Rainbow signout succeeded ")
                    val sharedPref = getPreferences(Context.MODE_PRIVATE)
                    AppData.getInstance().clearSignalsContact()
                }
            })

        }

    }

    private fun isPermissionsGranted(): Boolean {
        return (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)

    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),1000)

    }

    @SuppressLint("MissingPermission")
    private fun requestLocationUpdate() {
        val locationRequest = LocationRequest()

        if (isPermissionsGranted()) {
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            locationRequest.setInterval(120000)
//            locationRequest.setFastestInterval(60000)
            LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(locationRequest, mLocationCallback, Looper.getMainLooper())
            if (BuildConfig.DEBUG) Log.d(TAG, " requestLocationUpdate interval = ${locationRequest.interval} fastestInterval = ${locationRequest.fastestInterval}")
        }
    }

}