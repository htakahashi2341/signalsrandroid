package com.iconvn.signalsr

import java.util.*

class AppConstants {

    companion object {

        val CHANNEL_ID: String = "isgnalsr"
        val ALARM_NUMBER: Int = 4
        val ALARM_NAME: List<String> = listOf("LOCKOUT!", "LOCKDOWN!", "EVACUATE!", "SHELTER!")
        val ALARM_DESC: List<String> = listOf("Get inside. Lock outside doors.", "Locks, lights, out of sight.", "To the announced location.", "Hazard and safety strategy.")
        val ALARM_ICON: List<String> = listOf("ic_lockouot", "ic_lockdown", "ic_evacuate", "ic_shelter")

        val SIGNALS_CMD_ALARM_PLACE: List<String> = listOf("~lkot x", "~lkdn x", "~evac x", "~shel x")
        val SIGNALS_CMD_ALARM_CLEAR: List<String> = listOf("~lkot c", "~lkdn c", "~evac c", "~shel c")
        val SIGNALS_CMD_PARAM_SOURCE: String = "src="
        val SIGNALS_CMD_PARAM_LOC_MAP: String = "locmap="
        val SIGNALS_CMD_PARAM_LOC_LAT: String = "loclat="
        val SIGNALS_CMD_PARAM_LOC_LGT: String = "loclgt="


        val SIGNALS_RES_OK: String = "OK"
        val SIGNALS_RES_PARAM_EXECUTE: String = "op=x"
        val SIGNALS_RES_PARAM_CLEARED: String = "op=c"
        val SIGNALS_RES_PARAM_COMMAND_LKOT: String = "cmd=lkot"
        val SIGNALS_RES_PARAM_COMMAND_LKDN: String = "cmd=lkdn"
        val SIGNALS_RES_PARAM_COMMAND_EVAC: String = "cmd=evac"
        val SIGNALS_RES_PARAM_COMMAND_SHEL: String = "cmd=shel"

        val MAP_ALARM_COMMAND_NAME: Map<String, String> = mapOf("one" to "two", "two" to "three")

        val BUTTON_ALPHA_TRANS_25: Int = 64
        val BUTTON_ALPHA_OPAQUE:   Int = 255

        val KEY_APP_DATA: String = "key_data_app"
        val KEY_SIGNIN_EMAIL: String = "key_signin_email"
        val KEY_SIGNIN_PASSWORD: String = "key_signin_password"
        val KEY_SIGNALS_EMAIL: String = "key_signals_email"
        val KEY_SIGNALS_JID: String = "key_signals_jid"

        val TIMER_MS_RAINOW_SIGNIN: Long = 30000
        val TIMER_MS_ALARM:         Long = 30000
        val TIMER_MS_INTERVAL:      Long = 1000
        val TIMER_MS_AVATAR_DISP:   Long = 10000
        val TIMER_MS_SHAKE:         Long = 1000
        val TIMER_MS_SHAKE_INTERVAL: Long = 100



    }

}

