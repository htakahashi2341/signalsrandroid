package com.iconvn.signalsr.fragment.home

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.iconvn.signalsr.R


class HomeViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    interface ItemClickListener {
        fun onItemClick(view: View, position: Int)
    }

    val itemTextAlarmName: TextView = view.findViewById(R.id.textAlarmName)
    val itemImageAlarm: ImageView = view.findViewById(R.id.imageAlarm)
    val itemTextAlarmDescription: TextView = view.findViewById(R.id.textAlarmDescription)

}