package com.iconvn.signalsr.fragment.home

import android.location.Geocoder
import android.os.Bundle
import android.os.CountDownTimer
import android.transition.TransitionInflater
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ale.infra.contact.IRainbowContact
import com.ale.infra.list.IItemListChangeListener
import com.ale.infra.manager.IMMessage
import com.ale.infra.proxy.conversation.IRainbowConversation
import com.ale.listener.IConnectionChanged
import com.ale.listener.IRainbowImListener
import com.ale.listener.SigninResponseListener
import com.ale.rainbowsdk.RainbowSdk
import com.ale.rainbowx.rainbowavatar.AvatarCardView
import com.iconvn.signalsr.*

class HomeFragment : Fragment(), HomeViewHolder.ItemClickListener, IRainbowImListener, IConnectionChanged {
    private val TAG: String = ">>> HomeFragment"
    private val mColumnCount: Int = AppConstants.ALARM_NUMBER
    private var mAlarmTimer: CountDownTimer? = null
    private val mChangeListener = IItemListChangeListener(::searchSignalsContact)
    private var mSigninTimer: CountDownTimer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val inflater = TransitionInflater.from(requireContext())
        exitTransition = inflater.inflateTransition(R.transition.fade)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (BuildConfig.DEBUG) Log.d(TAG, "onCreateView")

        val view = inflater.inflate(R.layout.fragment_home, container, false)        // Set the adapter

        val textSignalsStatus: TextView = view.findViewById(R.id.textSignalsStatus)
        textSignalsStatus.background = ContextCompat.getDrawable(activity as MainActivity, R.drawable.tv_signals_status_ko)
        textSignalsStatus.text = getString(R.string.def_signals_status)

        val homeRecyclerView: RecyclerView = view.findViewById(R.id.homeRecycleView)
        if (homeRecyclerView is RecyclerView) {
            val itemClickListener: HomeViewHolder.ItemClickListener = this

            with(view) {
                homeRecyclerView.layoutManager = when {
                    mColumnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, mColumnCount)
                }

                homeRecyclerView.setHasFixedSize(true)

                homeRecyclerView.adapter =
                HomeRecyclerViewAdapter(
                    activity as MainActivity,
                    itemClickListener,
                    AppConstants.ALARM_NAME,
                    AppConstants.ALARM_DESC
                )
                homeRecyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            }

        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (BuildConfig.DEBUG) Log.d(TAG, "onViewCreated")

        RainbowSdk.instance().im().registerListener(this)
        RainbowSdk.instance().connection().registerConnectionChangedListener(this)

    }

    override fun onStart() {
        super.onStart()
        if (BuildConfig.DEBUG) Log.d(TAG, "onStart")
//        RainbowSdk.instance().im().registerListener(this)

        val signalsContact: IRainbowContact? = AppData.getInstance().getSignalsContact()
        if (signalsContact != null && RainbowSdk.instance().connection().isConnected) {
//            updateRainbowState()
            if (BuildConfig.DEBUG) Log.d(TAG, "onStart updateRainbowState() will be called by onResume instead")
        } else {
            if (AppData.getInstance().getSigninEmail() == null || AppData.getInstance().getSigninPassword() == null || AppData.getInstance().getSignalsEmail() == null ) {

                val builder = AppAlert.getInstance().createAlert(
                    activity as MainActivity,
                    getString(R.string.alert_title_no_valid_conf),
                    getString(R.string.alert_message_no_valid_conf),
                    android.R.drawable.ic_dialog_alert
                )

                builder.setPositiveButton(getString(R.string.alert_confirm_ok)) { dialogInterface, which ->
/*
                    val fragmentManager = activity?.supportFragmentManager
                    if (fragmentManager != null) {
                        val fragmentTransaction = fragmentManager.beginTransaction()
                        val fragment = fragmentManager.findFragmentById(R.id.settingsFragment)
                            fragmentTransaction.replace(R.id.containerFragment, SettingsFragment.newInstance())

                            fragmentTransaction.commit()

                    }
*/
                }
                activity?.runOnUiThread { builder.create().show() }
            }
        }
    }


    override fun onResume() {
        super.onResume()

        if (BuildConfig.DEBUG) Log.d(TAG, "onResume: isConnected= ${RainbowSdk.instance().connection().isConnected}")
        if (BuildConfig.DEBUG) Log.d(TAG, "onResume: isDisconnected= ${RainbowSdk.instance().connection().isDisconnected}")

        if (!RainbowSdk.instance().connection().isConnected &&
//            AppData.getInstance().getSigninEmail() != null && AppData.getInstance().getSigninPassword() != null && AppData.getInstance().getSignalsEmail() != null   ) {
            AppData.getInstance().getSigninEmail() != null && AppData.getInstance().getSigninPassword() != null && AppData.getInstance().getSignalsEmail() != null) {

                if (BuildConfig.DEBUG) Log.d(TAG, "onResume: Goto Auto Login")
                rainbowSignIn()
        } else if (RainbowSdk.instance().connection().isConnected) {
            updateRainbowState()
        }
    }

    private fun clearRainbowState() {
        if (BuildConfig.DEBUG) Log.d(TAG, "clearRainbowState")
        val textOrgName: TextView? = activity?.findViewById(R.id.textOrgName)
        textOrgName?.let {
            it.text = getString(R.string.def_org_name)
        }

        val textSignalsStatus: TextView? = activity?.findViewById(R.id.textSignalsStatus)
        textSignalsStatus?.let {
            it.background = ContextCompat.getDrawable(activity as MainActivity, R.drawable.tv_signals_status_ko)
            it.text = getString(R.string.def_signals_status)
        }

        val textLocationAddress: TextView? = activity?.findViewById(R.id.textLocationAddress1)
        textLocationAddress?.let {
            it.text = getString(R.string.def_location)
        }
        val textLocationAddress2: TextView? = activity?.findViewById(R.id.textLocationAddress2)
        textLocationAddress2?.let {
            it.text = getString(R.string.def_location2)
        }

    }

    private fun updateRainbowState() {

        if (BuildConfig.DEBUG) Log.d(TAG, "updateRainbowState")

        val progressBarSignIn: ProgressBar? = activity?.findViewById(R.id.progressBarSignIn)
        activity?.runOnUiThread { progressBarSignIn?.visibility = View.GONE }

        mAlarmTimer?.let {
            it.cancel()
        }
        mAlarmTimer = null

        val signalsContact: IRainbowContact? = AppData.getInstance().getSignalsContact()



        val geoCoder = Geocoder(activity)
        val latitude: Double? = AppData.getInstance().getLatitude()
        val longitude: Double? = AppData.getInstance().getLongitude()
        if (latitude != null && longitude != null) {
            try {
                val addressList = geoCoder.getFromLocation(AppData.getInstance().getLatitude()!!, AppData.getInstance().getLongitude()!!, 1)
                var addressLine1: String = ""
                val subThoroughfare: String? = addressList[0].subThoroughfare
                subThoroughfare?.let { addressLine1 = it }
                val thoroughfare: String? = addressList[0].thoroughfare
                thoroughfare?.let { addressLine1 = "$addressLine1 $it" }
                val locality: String? = addressList[0].locality
                locality?.let { addressLine1 = "$addressLine1 $it" }

                var addressLine2: String = ""
                val subAdminArea: String? = addressList[0].subAdminArea
                subAdminArea?.let { addressLine2 = it }
                val adminArea: String? = addressList[0].adminArea
                adminArea?.let { addressLine2 = "$addressLine2 $it" }
                val postalCode: String? = addressList[0].postalCode
                postalCode?.let { addressLine2 = "$addressLine2 $it" }
                val countryCode: String? = addressList[0].countryCode
                countryCode?.let { addressLine2 = "$addressLine2 $it" }

                val textLocationAddress: TextView? = activity?.findViewById(R.id.textLocationAddress1)
                textLocationAddress?.let {
                    it.text = addressLine1
                }
                val textLocationAddress2: TextView? = activity?.findViewById(R.id.textLocationAddress2)
                textLocationAddress2?.let {
                    it.text = addressLine2
                }
            } catch (e: Exception) {
                if (BuildConfig.DEBUG) Log.d(TAG, "updateRainbowState: Geocoder Exception")
            }

        }



        val textOrgName: TextView? = activity?.findViewById(R.id.textOrgName)
        if (RainbowSdk.instance().connection().isConnected) {
            if (BuildConfig.DEBUG) Log.d(TAG, "updateRainbowState Connected")
            val orgName: String? = RainbowSdk.instance().myProfile().getConnectedUser().companyName
            orgName?.let {
                if (BuildConfig.DEBUG) Log.d(TAG, "updateRainbowState companyName exists")
                textOrgName?.text = if (it.isNotEmpty()) it else getString(R.string.def_org_name)

            }

            val textSignalsStatus: TextView? = activity?.findViewById(R.id.textSignalsStatus)
            if (signalsContact != null) {
                val conversation: IRainbowConversation? = RainbowSdk.instance().conversations().getConversationFromContact(signalsContact.jid)
                if (BuildConfig.DEBUG) Log.d(TAG, "updateRainbowState: conversation?.id=${ conversation?.id}")
                if (conversation != null) {
                    AppData.getInstance().setSignalsConversation(conversation)
                    if (BuildConfig.DEBUG) Log.d(TAG, "updateRainbowState: getSignalsConversation=${AppData.getInstance().getSignalsConversation()}")
                    textSignalsStatus?.let {
                        it.background = ContextCompat.getDrawable(activity as MainActivity, R.drawable.tv_signals_status_ok)
                        it.text = signalsContact.firstName + " " + signalsContact.lastName + " " + getString(R.string.signals_status_ready_with_name)
                    }

                }
            } else {
                textSignalsStatus?.let {
                    if (BuildConfig.DEBUG) Log.d(TAG, "updateRainbowState No Signals Connected")
                    it.background = ContextCompat.getDrawable(activity as MainActivity, R.drawable.tv_signals_status_ko)
                    it.text = getString(R.string.def_signals_status)
                }
            }
            val avatar: AvatarCardView? = activity?.findViewById(R.id.cardConnectedAvatar)
            activity?.runOnUiThread { avatar?.displayContact(RainbowSdk.instance().myProfile().getConnectedUser()) }

        } else {
            if (BuildConfig.DEBUG) Log.d(TAG, "updateRainbowState Not Connected")
            textOrgName?.text = getString(R.string.def_org_name)
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (BuildConfig.DEBUG) Log.d(TAG, "onHiddenChanged: hidden=${hidden}")

    }

    override fun onDetach() {
        super.onDetach()
        if (BuildConfig.DEBUG) Log.d(TAG, "onDetach")

    }

    override fun onDestroy() {
        super.onDestroy()
        if (BuildConfig.DEBUG) Log.d(TAG, "onDestroy")
        mAlarmTimer?.cancel()
        mAlarmTimer = null
        mSigninTimer?.cancel()
        mSigninTimer = null

    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (BuildConfig.DEBUG) Log.d(TAG, "onDestroyView")
        RainbowSdk.instance().im().unregisterListener(this)
        RainbowSdk.instance().connection().unregisterConnectionChangedListener(this)

    }

    override fun onItemClick(view: View, position: Int) {

        if (BuildConfig.DEBUG) Log.d(TAG, "onItemClick: position=${position}")
//        val conversation: IRainbowConversation? = RainbowSdk.instance().conversations().getConversationFromContact(AppData.getInstance().getSignalsContact()?.jid)

        if  (mAlarmTimer != null) {
            return
        }

//        if (mSignalsConversation == null) {
        if (BuildConfig.DEBUG) Log.d(TAG, "onItemClick: getSignalsConversation=${AppData.getInstance().getSignalsConversation()}")
        if (AppData.getInstance().getSignalsConversation() == null ||
            !RainbowSdk.instance().connection().isConnected) {

            val builder = AppAlert.getInstance().createAlert(activity as MainActivity,
                                                             getString(R.string.alert_title_signals_not_connected),
                                                             getString(R.string.alert_message_no_valid_conf),
                                                             android.R.drawable.ic_dialog_alert)
            builder.setPositiveButton(getString(R.string.alert_confirm_ok)) {dialogInterface, which ->

            }
            activity?.runOnUiThread {builder.create().show()}
            return
        }

        val originator = RainbowSdk.instance().myProfile().getConnectedUser().firstName + " " + RainbowSdk.instance().myProfile().getConnectedUser().lastName
        val builder = AppAlert.getInstance().createAlert(activity as MainActivity,
                                                         "${AppConstants.ALARM_NAME[position]} ",
                                                         getString(R.string.alert_message_alarm_requesting) + " ${AppConstants.ALARM_NAME[position]}",
                                                         android.R.drawable.ic_dialog_alert)
        builder.setPositiveButton(getString(R.string.alert_confirm_confirm)) {dialogInterface, which ->
            Log.d(TAG," onItemClick CONFIRM signalsConversation= ${AppData.getInstance().getSignalsConversation()}")
            val geoCoder = Geocoder(context)

            val addressList = geoCoder.getFromLocation(AppData.getInstance().getLatitude()!!, AppData.getInstance().getLongitude()!!,1)

            RainbowSdk.instance().im().sendMessageToConversation(AppData.getInstance().getSignalsConversation(),
                                                       AppConstants.SIGNALS_CMD_ALARM_PLACE[position] +
                                                                 " ${AppConstants.SIGNALS_CMD_PARAM_SOURCE} $originator"  +
                                                                 " ${AppConstants.SIGNALS_CMD_PARAM_LOC_LAT} ${AppData.getInstance().getLatitude().toString()}" +
                                                                 " ${AppConstants.SIGNALS_CMD_PARAM_LOC_LGT} ${AppData.getInstance().getLongitude().toString()}" +
                                                                 " ${AppConstants.SIGNALS_CMD_PARAM_LOC_MAP} ${addressList[0].getAddressLine(0)}")

            val progressBarAlarm: ProgressBar? = activity?.findViewById(R.id.progressBarAlarm)
            activity?.runOnUiThread {progressBarAlarm?.visibility = View.VISIBLE}

            mAlarmTimer = object: CountDownTimer(AppConstants.TIMER_MS_ALARM, AppConstants.TIMER_MS_INTERVAL) {
                public override fun onTick(p0: Long) {
                    if (BuildConfig.DEBUG) Log.d(TAG, "onTick  TIMER_MS_ALARM p0= ${p0}")

                }

                public override fun onFinish() {
                    if (BuildConfig.DEBUG) Log.d(TAG, "onFinish")
                    activity?.runOnUiThread {progressBarAlarm?.visibility = View.GONE}
                    mAlarmTimer = null
                    val builder = AppAlert.getInstance().createAlert(activity as MainActivity,
                        getString(R.string.alert_title_signals_not_responding), getString(R.string.alert_message_signals_not_responding), android.R.drawable.ic_dialog_alert)

                    builder.setPositiveButton(getString(R.string.alert_confirm_ok)) {dialogInterface, which ->
                        if (BuildConfig.DEBUG) Log.d(TAG," onItemClick CONFIRM")
                    }
                    activity?.runOnUiThread { builder.create().show() }
                }

            }.start()


        }
/*  Alarm clear operation is hidden on the first release and will be available in the version2 for authorized users only
        builder.setNegativeButton(getString(R.string.alert_confirm_clear)) {dialogInterface, which ->
            Log.d(TAG," onItemClick CLEAR")
            RainbowSdk.instance().im().sendMessageToConversation(mSignalsConversation, AppConstants.SIGNALS_CMD_ALARM_CLEAR[position] + " by $originator")
            activity?.runOnUiThread {progressBar1?.visibility = View.VISIBLE}
        }
*/
        builder.setNeutralButton(getString(R.string.alert_confirm_cancel)) {dialogInterface, which ->
            if (BuildConfig.DEBUG) Log.d(TAG," onItemClick CANCEL")
        }

        activity?.runOnUiThread {builder.create().show()}

    }

    override fun onImSent(conversation: IRainbowConversation?, message: IMMessage?) {
        if (BuildConfig.DEBUG) Log.d(TAG," onImSent conversationId= ${conversation}")
    }

    override fun onImReceived(conversation: IRainbowConversation?, message: IMMessage?) {

        if (conversation?.id == AppData.getInstance().getSignalsConversation()?.id) {

            if (BuildConfig.DEBUG) Log.d(TAG," onImReceived conversation?.id= ${conversation?.id} message= ${message?.messageContent}")

            val progressBarAlarm: ProgressBar? = activity?.findViewById(R.id.progressBarAlarm)
            activity?.runOnUiThread {progressBarAlarm?.visibility = View.GONE}

            val receivedMessage: String? = message?.messageContent
            if (receivedMessage != null) {
                if (receivedMessage.indexOf(AppConstants.SIGNALS_RES_OK, 0) >=0) {

                    val alarmName: String

                    if (receivedMessage.indexOf(AppConstants.SIGNALS_RES_PARAM_COMMAND_LKOT, 0) >= 0) {
                        alarmName = AppConstants.ALARM_NAME[0]
                    } else if (receivedMessage.indexOf(AppConstants.SIGNALS_RES_PARAM_COMMAND_LKDN, 0) >= 0) {
                        alarmName = AppConstants.ALARM_NAME[1]
                    } else if (receivedMessage.indexOf(AppConstants.SIGNALS_RES_PARAM_COMMAND_EVAC, 0) >= 0) {
                        alarmName = AppConstants.ALARM_NAME[2]
                    } else if (receivedMessage.indexOf(AppConstants.SIGNALS_RES_PARAM_COMMAND_SHEL, 0) >= 0) {
                        alarmName = AppConstants.ALARM_NAME[3]
                    } else {
                        alarmName = "DUMMY"
                    }

                    mAlarmTimer?.cancel()
                    mAlarmTimer = null
                    val actionType: String
                    val alertMessage: String
                    if (receivedMessage.indexOf("op=x", 0) >= 0) {
                        actionType = getString(R.string.alert_title_alarm_executing)
                        alertMessage = getString(R.string.alert_message_alarm_executing)
                    } else {
                        actionType = getString(R.string.alert_title_alarm_cleared)
                        alertMessage = getString(R.string.alert_message_alarm_cleared)
                    }
                    Log.d(TAG," onImReceived Alert Title = ${alarmName + " " + actionType}")
                    val builder = AppAlert.getInstance().createAlert(activity as MainActivity,
                                                                    "$alarmName $actionType",
                                                                     alertMessage,
                                                                     android.R.drawable.ic_dialog_info)

                    builder.setPositiveButton(getString(R.string.alert_confirm_ok)) {dialogInterface, which ->
                    }

                    activity?.runOnUiThread {builder.create().show()}
                }
            } else {
                if (BuildConfig.DEBUG) Log.d(TAG," onImReceived conversation?.id= ${conversation?.id} Message NULL")
            }
        } else {
            if (BuildConfig.DEBUG) Log.d(TAG," onImReceived conversation?.id= ${conversation?.id} NOT a SIGNALS Conversation")
        }
    }

    fun rainbowSignIn() {

        val signInEmail = AppData.getInstance().getSigninEmail()
        val signInPassword = AppData.getInstance().getSigninPassword()

        if (BuildConfig.DEBUG) Log.d(TAG, "rainbowSignIn")
        mSigninTimer?.cancel()
        mSigninTimer = object :
            CountDownTimer(AppConstants.TIMER_MS_RAINOW_SIGNIN, AppConstants.TIMER_MS_INTERVAL) {
            public override fun onTick(p0: Long) {
                if (BuildConfig.DEBUG) Log.d(TAG, "onTick  TIMER_MS_RAINOW_SIGNIN p0= ${p0} isConnected= ${RainbowSdk.instance().connection().isConnected}")
                if (RainbowSdk.instance().connection().isConnected) {
                     mSigninTimer?.cancel()
                     mSigninTimer = null
                    val progressBarSignIn: ProgressBar? = activity?.findViewById(R.id.progressBarSignIn)
                    activity?.runOnUiThread {progressBarSignIn?.visibility = View.GONE }
                    updateRainbowState()
                }
            }

            public override fun onFinish() {
                if (BuildConfig.DEBUG) Log.d(TAG, "onFinish")
                val progressBarSignIn: ProgressBar? = activity?.findViewById(R.id.progressBarSignIn)
                activity?.runOnUiThread {progressBarSignIn?.visibility = View.GONE }
                val alertTitle = if (RainbowSdk.instance().connection().isConnected) getString(R.string.alert_title_signals_not_connected)
                else getString(R.string.alert_title_signin_failed)
                val alertMessage = if (RainbowSdk.instance().connection().isConnected) getString(R.string.alert_message_signals_not_connected)
                else getString(R.string.alert_message_signin_failed)
                val builder = AppAlert.getInstance().createAlert(activity as MainActivity, alertTitle, alertMessage, android.R.drawable.ic_dialog_alert)

                builder.setPositiveButton(getString(R.string.alert_confirm_ok)) { dialogInterface, which ->
                    Log.d(TAG, " onItemClick CONFIRM")
                    val textSignalsStatus: TextView? = activity?.findViewById(R.id.textSignalsStatus)
                    textSignalsStatus?.let {
                        activity?.runOnUiThread {
                            it.background = ContextCompat.getDrawable(activity as MainActivity, R.drawable.tv_signals_status_ko)
                            it.text = getString(R.string.signals_status_not_ready)
                        }
                    }
                    val textOrgName: TextView? = activity?.findViewById(R.id.textOrgName)
                    textOrgName?.let {
                        activity?.runOnUiThread {
                            it.text = getString(R.string.def_org_name)
                        }
                    }
                }
                activity?.runOnUiThread { builder.create().show() }
            }
        }.start()
        if (signInEmail == null || signInPassword == null) {
            return
        }
        if (BuildConfig.DEBUG) Log.d(TAG,"rainbowSignIn: email= ${signInEmail} password= ${signInPassword} ")
        val progressBarSignIn: ProgressBar? = activity?.findViewById(R.id.progressBarSignIn)
        activity?.runOnUiThread { progressBarSignIn?.visibility = View.VISIBLE }
        // Passing listener to start function
        RainbowSdk.instance().connection().signin(signInEmail, signInPassword, object : SigninResponseListener() {
            override fun onRequestFailed(p0: RainbowSdk.ErrorCode?, p1: String?) {
                if (BuildConfig.DEBUG) Log.d(TAG, "Rainbow signin Failed p0= ${p0} p1= ${p1}")
                activity?.runOnUiThread { progressBarSignIn?.visibility = View.GONE }
                mSigninTimer?.let {
                    it.cancel()
                }
                mSigninTimer = null

                    val builder = AppAlert.getInstance().createAlert(activity as MainActivity,
                        getString(R.string.alert_title_signin_failed),
                        getString(R.string.alert_message_signin_failed),
                        android.R.drawable.ic_dialog_alert)

                    builder.setPositiveButton(getString(R.string.alert_confirm_ok)) {dialogInterface, which ->
                        if (BuildConfig.DEBUG) Log.d(TAG," onItemClick CONFIRM")
                    }
                    activity?.runOnUiThread { builder.create().show() }
            }
            override fun onSigninSucceeded() {
                if (BuildConfig.DEBUG) Log.d(
                    TAG,
                    "Rainbow signin succeeded First and Last name= ${RainbowSdk.instance()
                        .myProfile().getConnectedUser().firstName} ${RainbowSdk.instance()
                        .myProfile().getConnectedUser().lastName}"
                )

                if (BuildConfig.DEBUG) Log.d(
                    TAG,
                    "Rainbow signin succeeded ID= ${RainbowSdk.instance()
                        .myProfile().getConnectedUser().id}"
                )

                RainbowSdk.instance().contacts().rainbowContacts.registerChangeListener(mChangeListener)
            }
        })
    }

    public fun updateLocationAddress(address: String) {
        val textLocationAddress: TextView? = activity?.findViewById(R.id.textLocationAddress1)
        textLocationAddress?.let {it.text = address}
    }

    private fun searchSignalsContact() {

        val signalsEmail = AppData.getInstance().getSignalsEmail()

        val contacts = mutableListOf<IRainbowContact>()
        contacts.clear()
        contacts.addAll(RainbowSdk.instance().contacts().rainbowContacts.copyOfDataList)

        if (BuildConfig.DEBUG) Log.d(TAG, "searchSignalsContact size= ${contacts.count()}")

        for (contact: IRainbowContact in contacts) {
            if (BuildConfig.DEBUG) Log.d(
                TAG,
                "searchSignalsContact ${contact.loginEmail} ${contact.mainEmailAddress} ${contact.lastName}  ${contact.id} ${contact.jid}"
            )
            if (contact.loginEmail == signalsEmail) {
                mSigninTimer?.let {
                    if (BuildConfig.DEBUG) Log.d(TAG, "searchSignalsContact() mSigninTimer was canceled")
                    it.cancel()
                }
                mSigninTimer = null
                AppData.getInstance().setSignalsContact(contact)
                RainbowSdk.instance().contacts().rainbowContacts.unregisterChangeListener(mChangeListener)
                updateRainbowState()

                break
            }
        }
    }


    override fun onMessagesListUpdated(p0: Int, conversation: IRainbowConversation?, p2: MutableList<IMMessage>?) {

    }

    override fun isTypingState(p0: IRainbowContact?, p1: Boolean, p2: String?) {

    }

    override fun onMoreMessagesListUpdated(p0: Int, conversation: IRainbowConversation?, p2: MutableList<IMMessage>?) {

    }

    override fun onConnectionSucceed() {
        if (BuildConfig.DEBUG) Log.d(TAG, "onConnectionSucceed")
        if (mSigninTimer != null) {
            return
        }
        updateRainbowState()

    }

    override fun onConnectionLost() {
        if (BuildConfig.DEBUG) Log.d(TAG, "onConnectionLost")
        clearRainbowState()

    }


}

