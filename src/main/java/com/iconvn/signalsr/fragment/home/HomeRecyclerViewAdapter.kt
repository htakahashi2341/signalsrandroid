package com.iconvn.signalsr.fragment.home

import android.content.Context
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.iconvn.signalsr.R


class HomeRecyclerViewAdapter(private val context: Context,
                              private val itemClickListener: HomeViewHolder.ItemClickListener,
                              private val itemListAlarmName: List<String>,
                              private val itemListAlarmDescription: List<String>
) : RecyclerView.Adapter<HomeViewHolder>()  {

    private val TAG: String = "HomeRecyclerViewAdapter"
    private var mRecyclerView: RecyclerView? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        mRecyclerView = recyclerView
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        mRecyclerView = null
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        holder?.let {
            Log.d(TAG,"nBindViewHolder: position= ${position}, name= ${itemListAlarmName.get(position)}")
            it.itemTextAlarmName.text = itemListAlarmName[position]

            it.itemTextAlarmDescription.text = itemListAlarmDescription.get(position)
            when (position) {
                0 -> it.itemImageAlarm.setImageResource(R.drawable.ic_lockout)
                1 -> it.itemImageAlarm.setImageResource(R.drawable.ic_lockdown)
                2 -> it.itemImageAlarm.setImageResource(R.drawable.ic_evacuate)
                3 -> it.itemImageAlarm.setImageResource(R.drawable.ic_shelter)
            }
        }
    }

    override fun getItemCount(): Int {
        return itemListAlarmName.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {

        val layoutInflater: LayoutInflater = LayoutInflater.from(context)
        val view: View = layoutInflater.inflate(R.layout.fragment_home_item, parent, false)
        view.setOnClickListener { view ->
            mRecyclerView?.let {
                itemClickListener.onItemClick(view, it.getChildAdapterPosition(view))
            }
        }
        return HomeViewHolder(view)
    }
}

