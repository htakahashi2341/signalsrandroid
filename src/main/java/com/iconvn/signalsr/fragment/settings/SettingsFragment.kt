package com.iconvn.signalsr.fragment.settings

import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.transition.TransitionInflater
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.ale.infra.contact.IRainbowContact
import com.ale.infra.list.IItemListChangeListener
import com.ale.listener.SigninResponseListener
import com.ale.listener.SignoutResponseListener
import com.ale.rainbowsdk.RainbowSdk
import com.github.razir.progressbutton.attachTextChangeAnimator
import com.github.razir.progressbutton.bindProgressButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.iconvn.signalsr.*

class SettingsFragment : Fragment() {

    private val TAG: String = ">>> SettingsFragment"
    private val mChangeListener = IItemListChangeListener(::getSignalsContact)
    private var mSigninTimer: CountDownTimer? = null

    val mButtonConnect: MaterialButton? = activity?.findViewById(R.id.buttonConnect)
    val mButtonDisconnect: MaterialButton? = activity?.findViewById(R.id.buttonDisconnect)
    val mEditYourEmail: TextInputEditText? = activity?.findViewById(R.id.editYourEmail)
    val mEditYourPassword: TextInputEditText? = activity?.findViewById(R.id.editYourPassword)
    val mEditSignalsEmail: TextInputEditText? = activity?.findViewById(R.id.editSignalsEmail)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (BuildConfig.DEBUG) Log.d(TAG, "onCreate")

        val inflater = TransitionInflater.from(requireContext())
        enterTransition = inflater.inflateTransition(R.transition.slide_right)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
//        if (BuildConfig.DEBUG) Log.d(TAG," onCreateView")
        Log.d(TAG," onCreateView")

        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (BuildConfig.DEBUG) Log.d(TAG," onViewCreated")

        mSigninTimer?.let {it.cancel()}
        mSigninTimer = null
        // Initialize the connect button
        val buttonConnect: Button = view.findViewById(R.id.buttonConnect)
        bindProgressButton(buttonConnect)
        buttonConnect.attachTextChangeAnimator()
        disableButton(buttonConnect)

        // Initialize the disconnect button
        val buttonDisconnect: Button = view.findViewById(R.id.buttonDisconnect)
        bindProgressButton(buttonDisconnect)
        buttonDisconnect.attachTextChangeAnimator()
        disableButton(buttonDisconnect)

        onClickListenerButtonDisconnect()
        onClickListenerButtonConnect()


        val editYourEmail: TextInputEditText = view.findViewById(R.id.editYourEmail)
        val editYourPassword: TextInputEditText = view.findViewById(R.id.editYourPassword)
        val editSignalsEmail: TextInputEditText = view.findViewById(R.id.editSignalsEmail)
        if (RainbowSdk.instance().connection().isConnected) {
            if (!RainbowSdk.instance().myProfile().getUserLoginInCache().isNullOrEmpty()) {
                editYourEmail.setText(RainbowSdk.instance().myProfile().getUserLoginInCache())
            }

            if (!RainbowSdk.instance().myProfile().getUserPasswordInCache().isNullOrEmpty()) {
                editYourPassword.setText(RainbowSdk.instance().myProfile().getUserPasswordInCache())
            }

            editSignalsEmail.setText(AppData.getInstance().getSignalsEmail())
            enableButton(buttonDisconnect)
        } else {
            editYourEmail.setText(AppData.getInstance().getSigninEmail())
            editYourPassword.setText(AppData.getInstance().getSigninPassword())
            editSignalsEmail.setText(AppData.getInstance().getSignalsEmail())
            if (validateEditFields()) {
                enableButton(buttonConnect)
            }

        }

        changeListenerEditYourEmail()
        changeListenerEditSignalsEmail()
        changeListenerEditYourPassword()

    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (BuildConfig.DEBUG) Log.d(TAG, "onHiddenChanged: hidden=${hidden}")

    }

    override fun onDestroy() {
        super.onDestroy()
        if (BuildConfig.DEBUG) Log.d(TAG, "onDestroy")
        mSigninTimer?.let {it.cancel()}
        mSigninTimer = null
    }

    override fun onDetach() {
        super.onDetach()
        if (BuildConfig.DEBUG) Log.d(TAG, "onDetach")

    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (BuildConfig.DEBUG) Log.d(TAG, "onDestroyView")
        RainbowSdk.instance().contacts().rainbowContacts.unregisterChangeListener(mChangeListener)

    }

    private fun changeListenerEditYourEmail() {
        val editYourEmail: TextInputEditText? = activity?.findViewById(R.id.editYourEmail)
        editYourEmail?.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (BuildConfig.DEBUG) Log.d(TAG, "afterTextChanged: p0= ${p0}")
                if (validateEditFields() && !RainbowSdk.instance().connection().isConnected) {
                    val buttonConnect: Button? = activity?.findViewById(R.id.buttonConnect)
                    buttonConnect?.let {enableButton(it)}
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (BuildConfig.DEBUG) Log.d(TAG, "beforeTextChanged: p0= ${p0} p1= ${p1} p2= ${p2} p3= ${p3}")

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (BuildConfig.DEBUG) Log.d(TAG, "onTextChanged: p0= ${p0} p1= ${p1} p2= ${p2} p3= ${p3}")
            }

        })

    }

    private fun changeListenerEditYourPassword() {
        val editYourPassword: TextInputEditText? = activity?.findViewById(R.id.editYourPassword)
        editYourPassword?.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (BuildConfig.DEBUG) Log.d(TAG, "afterTextChanged: p0= ${p0}")
                if (validateEditFields() && !RainbowSdk.instance().connection().isConnected) {
                    val buttonConnect: Button? = activity?.findViewById(R.id.buttonConnect)
                    buttonConnect?.let {enableButton(it)}
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (BuildConfig.DEBUG) Log.d(TAG, "beforeTextChanged: p0= ${p0} p1= ${p1} p2= ${p2} p3= ${p3}")

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (BuildConfig.DEBUG) Log.d(TAG, "onTextChanged: p0= ${p0} p1= ${p1} p2= ${p2} p3= ${p3}")
            }

        })

    }

    private fun changeListenerEditSignalsEmail() {
        val editSignalsEmail: TextInputEditText? = activity?.findViewById(R.id.editSignalsEmail)
        editSignalsEmail?.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (BuildConfig.DEBUG) Log.d(TAG, "afterTextChanged: p0= ${p0} validateEditFields= ${validateEditFields()}")
                if (validateEditFields() && !RainbowSdk.instance().connection().isConnected) {
                    val buttonConnect: Button? = activity?.findViewById(R.id.buttonConnect)
                    buttonConnect?.let {enableButton(it)}
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (BuildConfig.DEBUG) Log.d(TAG, "beforeTextChanged: p0= ${p0} p1= ${p1} p2= ${p2} p3= ${p3}")

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (BuildConfig.DEBUG) Log.d(TAG, "onTextChanged: p0= ${p0} p1= ${p1} p2= ${p2} p3= ${p3}")
            }

        })
    }

    private fun validateEditFields(): Boolean {

        val editYourEmail: TextInputEditText? = activity?.findViewById(R.id.editYourEmail)
        val editYourPassword: TextInputEditText? = activity?.findViewById(R.id.editYourPassword)
        val editSignalsEmail: TextInputEditText? = activity?.findViewById(R.id.editSignalsEmail)
        return editYourEmail?.text.toString().isNotEmpty()      &&
               editYourPassword?.text.toString().isNotEmpty()  &&
               editSignalsEmail?.text.toString().isNotEmpty()   &&
               AppUtl.getInstance().isEmailFormat(editYourEmail?.text.toString())  &&
               AppUtl.getInstance().isEmailFormat(editSignalsEmail?.text.toString())
    }

    private fun onClickListenerButtonConnect() {

        val buttonConnect: Button? = activity?.findViewById(R.id.buttonConnect)
        buttonConnect?.setOnClickListener {
            buttonConnect?.showProgress {
                progressColor = Color.WHITE
            }
            activity?.runOnUiThread { buttonConnect?.let{disableButton(it)} }

            mSigninTimer?.let {it.cancel()}
            mSigninTimer = object: CountDownTimer(AppConstants.TIMER_MS_RAINOW_SIGNIN, AppConstants.TIMER_MS_INTERVAL) {
                public override fun onTick(p0: Long) {
                    if (BuildConfig.DEBUG) Log.d(TAG, "onTick  p0= ${p0}")
                }

                public override fun onFinish() {
                    if (BuildConfig.DEBUG) Log.d(TAG, "onFinish")
                    activity?.runOnUiThread { buttonConnect?.hideProgress(getString(R.string.action_connect)) }
//                    activity?.runOnUiThread { disableButton(buttonConnect) }
                    val alertTitle = if (RainbowSdk.instance().connection().isConnected) getString(R.string.alert_title_signals_not_connected) else getString(R.string.alert_title_signin_failed)
                    val alertMessage = if (RainbowSdk.instance().connection().isConnected) getString(R.string.alert_message_signals_not_connected) else getString(R.string.alert_message_signin_failed)
                    val builder = AppAlert.getInstance().createAlert(activity as MainActivity,
                        alertTitle,
                        alertMessage,
                        android.R.drawable.ic_dialog_alert)

                    builder.setPositiveButton(getString(R.string.alert_confirm_ok)) {dialogInterface, which ->
                        if (BuildConfig.DEBUG) Log.d(TAG," onItemClick CONFIRM")
                        if (RainbowSdk.instance().connection().isConnected) {

                            RainbowSdk.instance().connection().signout(object : SignoutResponseListener() {

                                override fun onSignoutSucceeded() {
                                    if (BuildConfig.DEBUG) Log.d(TAG, "Rainbow signout succeeded ")
                                    AppData.getInstance().clearAll()
                                    clearAllEditFields()

                                }
                            })
                        }
                    }
                    activity?.runOnUiThread { builder.create().show() }
                }

            }.start()

            val editYourEmail: TextInputEditText? = activity?.findViewById(R.id.editYourEmail)
            val editYourPassword: TextInputEditText? = activity?.findViewById(R.id.editYourPassword)
            if (BuildConfig.DEBUG) Log.d(TAG, "onClickButtonConnect: email= ${editYourEmail?.text.toString()} password= ${editYourPassword?.text.toString()} ")
            // Passing listener to start function
            RainbowSdk.instance().connection().signin(editYourEmail?.text.toString(), editYourPassword?.text.toString(), object : SigninResponseListener() {

                override fun onRequestFailed(p0: RainbowSdk.ErrorCode?, p1: String?) {
                    if (BuildConfig.DEBUG) Log.d(TAG, "Rainbow signin Failed p0= ${p0} p1= ${p1}")

                    mSigninTimer?.let {it.cancel()}
                    mSigninTimer = null
                    activity?.runOnUiThread { buttonConnect.hideProgress(getString(R.string.action_connect)) }

                    val builder = AppAlert.getInstance().createAlert(activity as MainActivity,
                                                                     getString(R.string.alert_title_signin_failed),
                                                                     getString(R.string.alert_message_signin_failed),
                                                                     android.R.drawable.ic_dialog_alert)

                    builder.setPositiveButton(getString(R.string.alert_confirm_ok)) {dialogInterface, which ->
                        if (BuildConfig.DEBUG) Log.d(TAG," onItemClick CONFIRM")
                    }
                    activity?.runOnUiThread { builder.create().show() }

                }

                override fun onSigninSucceeded() {
                    if (BuildConfig.DEBUG) Log.d(TAG, "Rainbow signin succeeded First and Last name= ${RainbowSdk.instance().myProfile().getConnectedUser().firstName} ${RainbowSdk.instance().myProfile().getConnectedUser().lastName}")
                    if (BuildConfig.DEBUG) Log.d(TAG, "Rainbow signin succeeded ID= ${RainbowSdk.instance().myProfile().getConnectedUser().id}")

                    RainbowSdk.instance().contacts().rainbowContacts.registerChangeListener(mChangeListener)

                }
            })
        }
    }

    private fun onClickListenerButtonDisconnect() {
        val buttonDisconnect: Button? = activity?.findViewById(R.id.buttonDisconnect)
        buttonDisconnect?.setOnClickListener {
            buttonDisconnect?.showProgress {
                progressColor = Color.WHITE
            }
            if (BuildConfig.DEBUG) Log.d(TAG, "onClickButtonDisconnect: ")
            activity?.runOnUiThread { buttonDisconnect?.let {disableButton(it)} }

            // Passing listener to start function
            RainbowSdk.instance().connection().signout(object : SignoutResponseListener() {

                override fun onSignoutSucceeded() {
                    if (BuildConfig.DEBUG) Log.d(TAG, "Rainbow signout succeeded ")
                    AppData.getInstance().clearAll()
                    clearAllEditFields()
                    activity?.runOnUiThread { buttonDisconnect.hideProgress(getString(R.string.action_disconnect)) }
                    activity?.runOnUiThread {disableButton(buttonDisconnect)}
                }
            })
        }
    }
    private fun enableButton(button: Button) {
        button.isEnabled = true
        button.background.alpha = AppConstants.BUTTON_ALPHA_OPAQUE
        when (button.id) {
            R.id.buttonConnect    ->
                activity?.getColor(R.color.colorButtonConnectTextEnabled)?.let { button.setTextColor(it) }
            R.id.buttonDisconnect ->
                activity?.getColor(R.color.colorButtonDisconnectTextEnabled)?.let { button.setTextColor(it) }

        }
    }

    private fun disableButton(button: Button) {
        button.isEnabled = false
        button.background.alpha = AppConstants.BUTTON_ALPHA_TRANS_25

        when (button.id) {
            R.id.buttonConnect    ->
                if ((resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) == Configuration.UI_MODE_NIGHT_YES) {

                    activity?.getColor(R.color.colorButtonConnectTextDisabledDark)?.let {
                        if (BuildConfig.DEBUG) Log.d(TAG, "buttonConnectDisabledDark")
                        button.setTextColor(it) }
                } else {
                    if (BuildConfig.DEBUG) Log.d(TAG, "buttonConnectDisabledNoDark")
                    activity?.getColor(R.color.colorButtonConnectTextDisabled)?.let { button.setTextColor(it) }
                }
            R.id.buttonDisconnect ->
                if ((resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) == Configuration.UI_MODE_NIGHT_YES) {
                    if (BuildConfig.DEBUG) Log.d(TAG, "buttonDisconnectDisabledDark")
                    activity?.getColor(R.color.colorButtonDisconnectTextDisabledDark)?.let { button.setTextColor(it) }
                } else {
                    if (BuildConfig.DEBUG) Log.d(TAG, "buttonDisconnectDisabledNoDark")
                    activity?.getColor(R.color.colorButtonDisconnectTextDisabled)?.let { button.setTextColor(it) }
                }
        }
    }


    private fun clearAllEditFields() {
        val editYourEmail: TextInputEditText? = activity?.findViewById(R.id.editYourEmail)
        editYourEmail?.let{it.setText("")}

        val editYourPassword: TextInputEditText? = activity?.findViewById(R.id.editYourPassword)
        editYourPassword?.let{it.setText("")}

        val editSignalsEmail: TextInputEditText? = activity?.findViewById(R.id.editSignalsEmail)
        editSignalsEmail?.let{it.setText("")}
    }

    private fun getSignalsContact() {

        val contacts = mutableListOf<IRainbowContact>()
        contacts.clear()
        contacts.addAll(RainbowSdk.instance().contacts().rainbowContacts.copyOfDataList)

        if (BuildConfig.DEBUG) Log.d(TAG, "getSignalsContact size= ${contacts.count()}")

        val editSignalsEmail: TextInputEditText? = activity?.findViewById(R.id.editSignalsEmail)
        for (contact: IRainbowContact in contacts) {
            if (BuildConfig.DEBUG) Log.d(TAG, "getSignalsContact ${contact.loginEmail} ${contact.mainEmailAddress} ${contact.lastName}  ${contact.id} ${contact.jid}")
            if (contact.loginEmail == editSignalsEmail?.text.toString()) {
                mSigninTimer?.let {it.cancel()}
                mSigninTimer = null
                AppData.getInstance().setSignalsContact(contact)
                RainbowSdk.instance().contacts().rainbowContacts.unregisterChangeListener(mChangeListener)

                val buttonDisconnect: Button? = activity?.findViewById(R.id.buttonDisconnect)
                activity?.runOnUiThread { buttonDisconnect?.let {enableButton(it)}}

                val buttonConnect: Button? = activity?.findViewById(R.id.buttonConnect)
                activity?.runOnUiThread { buttonConnect?.hideProgress(getString(R.string.action_connect)) }
//                activity?.runOnUiThread { buttonConnecet?.let {disableButton(it)} }

                val builder = AppAlert.getInstance().createAlert(activity as MainActivity,
                    getString(R.string.alert_title_signin_and_connected),
                    getString(R.string.alert_message_signals_connected),
                    android.R.drawable.ic_dialog_info)

                builder.setPositiveButton(getString(R.string.alert_confirm_ok)) {dialogInterface, which ->
                    if (BuildConfig.DEBUG) Log.d(TAG," onItemClick OK")
                }
                activity?.runOnUiThread { builder.create().show() }
                val editYourEmail: TextInputEditText? = activity?.findViewById(R.id.editYourEmail)
                AppData.getInstance().setSigninEmail(editYourEmail?.text.toString())

                val editYourPassword: TextInputEditText? = activity?.findViewById(R.id.editYourPassword)
                AppData.getInstance().setSigninPassword(editYourPassword?.text.toString())
                AppData.getInstance().setSignalsEmail(editSignalsEmail?.text.toString())
                break
            }
        }
    }

    companion object {
        internal fun newInstance(): SettingsFragment {
            var settingsFragment = SettingsFragment()
            return settingsFragment
        }
    }

}