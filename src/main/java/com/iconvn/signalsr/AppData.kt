package com.iconvn.signalsr

import com.ale.infra.contact.Contact
import com.ale.infra.contact.IRainbowContact
import com.ale.infra.http.adapter.concurrent.RainbowServiceException
import com.ale.infra.manager.Conversation
import com.ale.infra.proxy.conversation.IRainbowConversation
import com.ale.listener.IRainbowContactsSearchListener
import com.ale.rainbowsdk.RainbowSdk
import com.iconvn.signalsr.*

class AppData {
    private var mSignalsContact: IRainbowContact? = null
    private var mSignalsConversation: IRainbowConversation? = null
    private var mSigninEmail: String? = null
    private var mSigninPassword: String? = null
    private var mSignalsEmail: String? = null
    private var mSignalsJid:String? = null
    private var mLatitude: Double? = null
    private var mLongitude: Double? = null
    private var mLocationTrack: Boolean = false

    companion object {
        private var instance: AppData? = null

        fun getInstance() = instance ?: synchronized(this) {
            instance ?: AppData().also { instance = it }
        }

    }

    fun setSignalsContact(contact: IRainbowContact?) {
        mSignalsContact = contact
    }

    fun getSignalsContact(): IRainbowContact? {
        return mSignalsContact
    }

    fun clearSignalsContact() {
        mSignalsContact = null
    }

    fun setSignalsConversation(conversation: IRainbowConversation?) {
        mSignalsConversation = conversation
    }

    fun getSignalsConversation(): IRainbowConversation? {
        return mSignalsConversation
    }

    fun clearSignalsConversation() {
        mSignalsConversation = null
    }

    fun setSigninEmail(email: String?) {
        mSigninEmail = email
    }

    fun getSigninEmail(): String? {
        return mSigninEmail
    }

    fun clearSigninEmail() {
        mSigninEmail = null
    }

    fun setSigninPassword(password: String?) {
        mSigninPassword = password
    }

    fun getSigninPassword(): String? {
        return mSigninPassword
    }

    fun clearSigninPassword() {
        mSigninPassword = null
    }

    fun setSignalsEmail(email: String?) {
        mSignalsEmail = email
    }

    fun getSignalsEmail(): String? {
        return mSignalsEmail
    }

    fun clearSignalsEmail() {
        mSignalsEmail = null
    }

    fun setSignalsJid(jid: String?) {
        mSignalsJid = jid
    }

    fun getSignalsJid(): String? {
        return mSignalsJid
    }

    fun clearSignalsJid() {
        mSignalsJid = null
    }

    fun getLatitude(): Double? {
        return mLatitude
    }

    fun setLatitude(latitude: Double?) {
        mLatitude = latitude
    }

    fun getLongitude(): Double? {
        return mLongitude
    }

    fun setLongitude(longitude: Double?) {
        mLongitude = longitude
    }

    fun enableLocationTrack() {
        mLocationTrack = true
    }

    fun disableLocationTrack() {
        mLocationTrack = false
    }

    fun isLocationTrackEnabled(): Boolean {
        return mLocationTrack
    }

    fun clearAllSignals() {
        clearSignalsEmail()
        clearSignalsContact()
        clearSignalsConversation()
        clearSignalsJid()
    }

    fun clearAllSignin() {
        clearSigninEmail()
        clearSigninPassword()
    }

    fun clearAll() {
        clearAllSignals()
        clearAllSignals()
    }
}