package com.iconvn.signalsr

import android.Manifest
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Looper
import android.provider.Settings
import android.util.Log
import androidx.core.app.ActivityCompat
import com.ale.infra.contact.Contact
import com.ale.infra.contact.IRainbowContact
import com.ale.infra.http.adapter.concurrent.RainbowServiceException
import com.ale.infra.list.IItemListChangeListener
import com.ale.listener.IRainbowContactsSearchListener
import com.ale.rainbowsdk.RainbowSdk
import com.github.razir.progressbutton.hideProgress
import com.iconvn.signalsr.*

@Suppress("DEPRECATION")


class SplashActivity : AppCompatActivity() {
    private val TAG: String = ">>> SplashActivity"
    private val SPLASH_TIME_OUT: Long = 3000
    private val appID: String = "4ed72df03c7011eaa4d397fea1040d8f"
    private val appSecret: String = "cRB2K08aJjVk5AYYQMwTWogG20nqJLeXme6SuMOP5vK5c9hLbFBGgH4JZlIl4YaR"
    private var mIsInitialStart: Boolean = true
    private var mIsStarted: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (BuildConfig.DEBUG) Log.d(TAG, "onCreate()")
        setContentView(R.layout.activity_splash)

        val sharedPreferences: SharedPreferences = getSharedPreferences(AppConstants.KEY_APP_DATA, Context.MODE_PRIVATE)
        AppData.getInstance().setSigninEmail(sharedPreferences.getString(AppConstants.KEY_SIGNIN_EMAIL, null))
        AppData.getInstance().setSigninPassword(sharedPreferences.getString(AppConstants.KEY_SIGNIN_PASSWORD, null))
        AppData.getInstance().setSignalsEmail(sharedPreferences.getString(AppConstants.KEY_SIGNALS_EMAIL, null))

        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), 1000)
        val signalsJid = sharedPreferences.getString(AppConstants.KEY_SIGNALS_JID, null)
        val intent = Intent(this, MainActivity::class.java)

/*
        if (RainbowSdk.instance().connection().isConnected && signalsJid != null) {
            Log.d(TAG, "onCreate isConnected= ${RainbowSdk.instance().connection().isConnected} ")
            val contacts = mutableListOf<IRainbowContact>()
            contacts.clear()
            contacts.addAll(RainbowSdk.instance().contacts().rainbowContacts.copyOfDataList)
            for (contact: IRainbowContact in contacts) {
                Log.d(TAG,"getSignalsContact ${contact.loginEmail} ${contact.mainEmailAddress} ${contact.lastName}  ${contact.id} ${contact.jid}")
                if (contact.jid == signalsJid) {
                    AppData.getInstance().setSignalsContact(contact)
                    break
                }
            }
            Handler().postDelayed({
                startActivity(intent)
                finish()
            }, SPLASH_TIME_OUT)


            RainbowSdk.instance().contacts().searchByJid(signalsJid, object: IRainbowContactsSearchListener {
                override fun searchStarted() {
                    Log.d(TAG, "IRainbowContactsSearchListener searchStarted")

                }

                override fun searchError(p0: RainbowServiceException?) {
                    Log.d(TAG, "IRainbowContactsSearchListener searchError")

                }

                override fun searchFinished(p0: MutableList<Contact>?) {
                    val contacts = mutableListOf<IRainbowContact>()
                    contacts.clear()
                    if (p0 != null && p0.size > 0) {
                        contacts.addAll(RainbowSdk.instance().contacts().rainbowContacts.copyOfDataList)
//                        contacts.addAll(p0 as MutableList<IRainbowContact>)
                        AppData.getInstance().setSignalsContact(contacts[0])
                    } else {
                        AppData.getInstance().clearSignalsContact()
                    }
                    Log.d(TAG, "IRainbowContactsSearchListener searchFinished Size= ${p0?.size} Signals= ${AppData.getInstance().getSignalsContact()?.loginEmail}")


                    Looper.prepare()
                    Handler().postDelayed({
                        startActivity(intent)
                        finish()
                    }, SPLASH_TIME_OUT)
                    Looper.loop()
                }

            })

        } else {
            Log.d(TAG, "onCreate isConnected= ${RainbowSdk.instance().connection().isConnected} ")
            RainbowSdk.instance().connection().start(object : StartResponseListener() {
                override fun onStartSucceeded() {
                    Log.d(TAG, "onCreate Connection Succeeded isConnected= ${RainbowSdk.instance().connection().isConnected} ")
                    Handler().postDelayed({
                        startActivity(intent)
                        finish()
                    }, SPLASH_TIME_OUT)
                }

                override fun onRequestFailed(p0: RainbowSdk.ErrorCode?, p1: String?) {
                    Log.v(TAG, " Connection Failed")
                    Handler().postDelayed({
                        startActivity(intent)
                        finish()
                    }, SPLASH_TIME_OUT)

                }
            })

        }

*/
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            1000 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                }
            }
        }

    }
    override fun onStop() {
        super.onStop()
        if (BuildConfig.DEBUG) {Log.v(TAG, ": onStop ")}
    }

    override fun onPause() {
        super.onPause()
        if (BuildConfig.DEBUG) {Log.v(TAG, ": onPause ")}
    }

    override fun onResume() {
        super.onResume()
        if (BuildConfig.DEBUG) {Log.v(TAG, ": onResume ")}

        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
            checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
            !mIsStarted) {
            if (BuildConfig.DEBUG) {Log.v(TAG, ": onResume Permissions All Granted ")}
            mIsStarted = true
            startApp()
        } else {
            if (!mIsInitialStart) {
                promptPermission()
            } else {
                mIsInitialStart = false
            }
        }
    }

    private fun startApp() {
        RainbowSdk.instance().initialize(this, appID, appSecret)
        val intent = Intent(this, MainActivity::class.java)

        Handler().postDelayed({
            startActivity(intent)
            finish()
        }, SPLASH_TIME_OUT)

//        startActivity(intent)
    }

    private fun promptPermission() {
        val builder = AppAlert.getInstance().createAlert(this,
            getString(R.string.alert_title_permission_not_granted),
            getString(R.string.alert_message_permission_not_granted),
            android.R.drawable.ic_dialog_alert)

        builder.setPositiveButton(getString(R.string.alert_confirm_ok)) {dialogInterface, which ->
            startActivity(Intent(Settings.ACTION_PRIVACY_SETTINGS))
        }
        runOnUiThread { builder.create().show()}

    }

}