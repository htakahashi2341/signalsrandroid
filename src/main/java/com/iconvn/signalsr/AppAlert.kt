package com.iconvn.signalsr

import android.app.AlertDialog
import android.content.Context

class AppAlert {
    companion object {
        private var instance: AppAlert? = null

        fun getInstance() = instance ?: synchronized(this) {
            instance ?: AppAlert().also { instance = it }
        }

    }

    fun createAlert(context: Context, alertTitle: String, alertMessage: String,alertIcon: Int): AlertDialog.Builder {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(alertTitle)
        builder.setMessage(alertMessage)
        builder.setIcon(alertIcon)
        return builder
    }

}